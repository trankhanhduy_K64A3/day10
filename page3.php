<?php
    session_start();

    define("true_answer", [
        "answer_1" => "a",
        "answer_2" => "b",
        "answer_3" => "c",
        "answer_4" => "d",
        "answer_5" => "a",
        "answer_6" => "b",
        "answer_7" => "c",
        "answer_8" => "d",
        "answer_9" => "a",
        "answer_10" => "b",
    ]);

    $questions = [
        "Câu hỏi 1" => ["A1","B1","C1","D1"],
        "Câu hỏi 2" => ["A2","B2","C2","D2"],
        "Câu hỏi 3" => ["A3","B3","C3","D3"],
        "Câu hỏi 4" => ["A4","B4","C4","D4"],
        "Câu hỏi 5" => ["A5","B5","C5","D5"],
        "Câu hỏi 6" => ["A6","B6","C6","D6"],
        "Câu hỏi 7" => ["A7","B7","C7","D7"],
        "Câu hỏi 8" => ["A8","B8","C8","D8"],
        "Câu hỏi 9" => ["A9","B9","C9","D9"],
        "Câu hỏi 10" => ["A10","B10","C10","D10"]
    ];

    $answer_labels = ["a", "b", "c", "d"];

    $count = 0;

    foreach (true_answer as $key => $answer) {
        if ($answer == $_SESSION[$key])
            $count++;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/page3.css">
</head>
<body>

    <div class="wrapper">
        <h2 style="text-align: center;">Kết quả</h2>
        <?php

        if (true) {
            echo "Bạn làm đúng " . $count . " câu " ."</br>";
        }
        if ($count < 4) {
            echo "Bạn quá kém, cần ôn tập thêm";
        }
        elseif ($count < 7) {
            echo "Cũng bình thường";
        }
        else {
            echo "Sắp sửa làm được trợ giảng lớp PHP";
        }
        ?>

<?php
                $count = 1;
                foreach ($questions as $question => $answers) {
                    $green_answer = true_answer['answer_' . $count];
                    $user_answer = $_SESSION['answer_' . $count];
                    if ($user_answer == $green_answer) {
                        $user_answer == "f";
                }
                ?>
                    <div class="question-box">
                        <div class="question-filed"><?php echo "Question " . $count . ": " . $question ?></div>
                        <?php
                        for ($i = 0; $i < sizeof($answers); $i++) {
                            $label = '';
                            if ($answer_labels[$i] == $green_answer) {
                                $label = 'green-answer';
                            }
                            else if (($answer_labels[$i] == $user_answer)) {
                                $label = 'red-answer';
                            }
                        ?>
                            <div class="answer-field">
                            <span class="dot <?php echo $label; ?>"></span>
                            <label class="answer-label <?php echo $label; ?>"><?php echo $answers[$i] ?></label>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                $count++;
                }
                ?>
    </div>
</body>
</html>