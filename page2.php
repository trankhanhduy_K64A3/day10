<?php
    session_start();

    $questions = [
        "Câu hỏi 6" => ["A6","B6","C6","D6"],
        "Câu hỏi 7" => ["A7","B7","C7","D7"],
        "Câu hỏi 8" => ["A8","B8","C8","D8"],
        "Câu hỏi 9" => ["A9","B9","C9","D9"],
        "Câu hỏi 10" => ["A10","B10","C10","D10"]
    ];
    $answer_labels = ["a", "b", "c", "d"];

    if (isset($_POST['submit'])) {
        for ($i = 6; $i <= 10; $i++) {
            if (isset($_POST['answer_' . $i])) {
                $_SESSION['answer_' . $i] = $_POST['answer_' . $i];
            }
            else {
                $_SESSION['answer_' . $i] = "e";
            }
        }
        header('Location: '.'page3.php');
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="wrapper">
        <form action="" method="post">
            <h2 style="text-align:center ;">Trắc nghiệm trang 2</h2>
                <?php
                $count = 6;
                foreach ($questions as $question => $answers) {
                ?>
                    <div class="question-box">
                        <div class="question-filed"><?php echo "Question " . $count . ": " . $question ?></div>
                        <?php
                        for ($i = 0; $i < sizeof($answers); $i++) {
                        ?>
                            <div class="answer-field">
                                <input type="radio" name=<?php echo "answer_" . $count ?> value=<?php echo $answer_labels[$i] ?>  id=<?php echo "answer_" . $count . $answer_labels[$i]; ?>>
                                <label for=<?php echo "answer_" . $count . $answer_labels[$i]; ?>><?php echo $answers[$i] ?></label>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                $count++;
                }
                ?>
                <button name="submit" id="button-submit" class="button-submit" type="submit">Nộp bài</button>
        </form> 
    </div>
</body>
</html>